﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Threading;
using System.IO;

namespace SAF_uzdevums_user_interface
{
    public partial class Form1 : Form
    {
        System.Threading.Timer updateTimer;
        private string serviceName = "IntergraService_Demo";
        private DateTime timeStampForCommand;
        private enum Command
        {
            Info = 128,
        }
        
        public Form1()
        {

            InitializeComponent();
    
        }
        private void CheckUpdate(object state)
        {
            string path = @"C:\Intergra_demo\info.txt";
            DateTime dt = File.GetLastWriteTime(path);

            if (dt > timeStampForCommand)
            {
                string[] lines = File.ReadAllLines(path);
                richTextBox1.Invoke(new Action(() => 
                { 
                    richTextBox1.Text = string.Concat(lines); 
                }));
                button1.Invoke(new Action(() =>
                {
                    button1.Enabled = true;
                }));
                updateTimer.Dispose();
            }
            else if (timeStampForCommand.AddSeconds(3) < DateTime.Now)
            {
                button1.Invoke(new Action(() =>
                {
                    button1.Enabled = true;
                }));
                updateTimer.Dispose();
            }
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            RunCommand(Command.Info);
        }

        /// <summary>
        /// Send command to Integra via serial PORT
        /// </summary>
        /// <param name="v"></param>
        private void RunCommand(Command command)
        {
            //
            //richTextBox1.Text += string.Format("Running command '{0}': ", command.ToString());
   
            var service = new ServiceController(serviceName);
            if(service == null)
            {
                richTextBox1.Text += string.Format("service not found\n");
                return;
            }
            if(service.Status == ServiceControllerStatus.Stopped)
            {
                richTextBox1.Text += string.Format("Failed - service '{0}' is not running\n", serviceName);
                return;
            }
            try
            {
                timeStampForCommand = DateTime.Now;
                service.ExecuteCommand((int)command);
                //richTextBox1.Text += string.Format("OK\n");
                updateTimer = new System.Threading.Timer(new TimerCallback(CheckUpdate), null, 0, 1000);
                button1.Enabled = false;
            } 
            catch(Exception e)
            {
                richTextBox1.Text += string.Format( e.Message + "\n");
            }
            
            

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
